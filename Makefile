program: server client

server: server.cpp 
	g++ -std=c++11 -fno-stack-protector -z execstack -no-pie -w -g server.cpp -o server

client: client.cpp
	g++ -std=c++11 -fno-stack-protector -z execstack -no-pie -w -g client.cpp -o client

clean:
	rm -f client server