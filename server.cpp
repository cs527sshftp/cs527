#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/param.h>
#include <sys/signal.h>
#include <sys/wait.h>
#include <netinet/tcp.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <random>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include <unordered_map>
#include <sys/stat.h>
#include <dirent.h>
using namespace std;

static int cancel_download;

void error(string message)
{
    cout<<"Error: "<<message<<endl;    
}

void cancel(int signal) 
{
    if(signal == SIGUSR1)
        cancel_download = 1;    
}

void handler(int signal) 
{    	
	while (waitpid((pid_t)(-1), 0, WNOHANG) > 0) {} 
}

int fileSize(const char* filename)
{
    std::ifstream in(filename, std::ifstream::ate);
    std::streampos fsize = in.tellg();
    return fsize;
}

bool checkIPAddress(const string &IPAddress)
{
    struct sockaddr_in sa;
    int result = inet_pton(AF_INET, IPAddress.c_str(), &(sa.sin_addr));
    return result != 0;
}

std::string randomString()
{
     std::string str("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz");
     std::random_device rd;
     std::mt19937 generator(rd());
     std::shuffle(str.begin(), str.end(), generator);
     return str;
}

static inline void ltrim(std::string &str) {
    str.erase(str.begin(), std::find_if(str.begin(), str.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

static inline void rtrim(std::string &str) {
    str.erase(std::find_if(str.rbegin(), str.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), str.end());
}

static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

void configMap(std::unordered_map<std::string,std::string>& userdb, std::unordered_map<std::string,pair<string,bool>>& aliasdb, int &port_no, string &base_dir)
{
	std::ifstream infile("sploit.conf");
	std::string line;
	while(getline(infile,line))
	{
		if(line.find("user") == 0)
		{				
			std::istringstream iss(line);
			std::string user, username, password;
			if(!(iss >> user >> username >> password)) return;
			userdb[username] = password;
		}
		else if(line.find("alias") == 0)
		{
			std::istringstream iss(line);
			std::string alias, aliasname, aliascommand;
			iss >> alias >> aliasname;
			int len = alias.length()+aliasname.length()+2;
			aliascommand = line.substr(len);
	        
	        if(aliascommand.find_first_of("$'&<>;") != string::npos || 
	            aliascommand.find("||") != string::npos)	        
	            aliasdb[aliasname].second = false;	        
	        else 	        
				aliasdb[aliasname].second = true;
	        
	        aliasdb[aliasname].first = aliascommand;
		}
		else if(line.find("port") == 0)
		{
			std::istringstream iss(line);
			std::string port;			
			iss >> port >> port_no;
		}
		else if(line.find("base") == 0)
		{
			std::istringstream iss(line);
			std::string base;
			iss >> base >> base_dir;
		}
	}
	infile.close();
}

void getClientInfo(string &clientInfo, int &sock_id_new, int sock_id, sockaddr_in c_addport, string &sidRecv, string &cmd, string &args)
{
	int n;	
	socklen_t szaddr = sizeof(c_addport);
	sock_id_new = accept(sock_id, (struct sockaddr *) &c_addport, &szaddr);		
	if(sock_id_new <= 0)		
	{
		error("socket accept failed!");
		exit(0);
	}

	int readLength = 0;
	while(true)
	{
		char internal[100];
		bzero(internal,100);
		n = read(sock_id_new,internal,sizeof(internal));
		//printf(internal);				
		clientInfo += internal;
		readLength += n;
		if(n < 100) break;
	}

	if(readLength <= 0)
	{
		error("passed 0 bytes!");
		exit(0);
	}
	
	clientInfo.resize(readLength);
    int pos1 = clientInfo.find_first_of("$");
    sidRecv = clientInfo.substr(0,pos1);
    int pos2 = clientInfo.find_first_of("$",pos1+1);	    
    cmd = clientInfo.substr(pos1+1,pos2-pos1-2);        
	if(pos2+1 != std::string::npos)
    	args = clientInfo.substr(pos2+1);    

    return;
}

int main(int argc, char *argv[])
{		
	if(argc > 1)
	{	
		/*char givenArgs[10];
		for(int i=1; i<argc; i++)
		{
			if(strlen(argv[i]) < 10) 
			{
				sprintf(givenArgs,argv[i]);					
			}
		}	*/	
		cout<<"ERROR: received unexpected arguments"<<endl;		
		return 0;
	}

	pid_t k;       													
	int n,port_no;  	
	string base_dir;
	signal(SIGUSR1, cancel);

	std::unordered_map<std::string,std::string> userdb;
	std::unordered_map<std::string,pair<string,bool>> aliasdb;	
	configMap(userdb,aliasdb,port_no,base_dir);		
	
	std::string username;
	std::unordered_map<std::string,std::pair<std::string,std::string> > sessiondb;
 	std::unordered_map<std::string,std::pair<int, int> > portdb;
    std::unordered_map<std::string, int > statusdb;

	bool passFlag = false;
	int aliasValid = 0;		

	int sock_id,sock_id_new;
	struct sockaddr_in s_addport,c_addport;		
	bzero((char *) &s_addport, sizeof(s_addport));	
	bzero((char *) &c_addport, sizeof(c_addport));
	
	sock_id = socket(AF_INET, SOCK_STREAM, 0);
	
	if(sock_id <= 0)
		error("socket setup failed!");

	s_addport.sin_family = AF_INET;
	s_addport.sin_port = htons(port_no);
	s_addport.sin_addr.s_addr = htonl(INADDR_ANY);
    
    if(bind(sock_id, (struct sockaddr *) &s_addport, sizeof(s_addport)) == -1)
    {
    	error("socket bind failed");
    	exit(0);
    }

	if(listen(sock_id, 5) != 0)
	{
		error("socket listen failed!");
		exit(0);
	}

	while(1)
	{
		/*std::cout<<"\nSessiondb table:"<<std::endl;
		for(auto i:sessiondb)		
			std::cout<<i.first<<"::"<<i.second.first<<"::"<<i.second.second<<std::endl;		
		std::cout<<std::endl; */
		/*std::cout<<"\nSessiondb table:"<<std::endl;
		for(auto i:aliasdb)		
			std::cout<<i.first<<"::"<<i.second.first<<"::"<<i.second.second<<std::endl;		
		std::cout<<std::endl;*/

		int saved_stdout = dup(1);				
		std::string clientInfo,sidRecv,cmd,args;
		getClientInfo(clientInfo,sock_id_new,sock_id,c_addport,sidRecv,cmd,args);
		trim(cmd); trim(args);		
		bool aliasFlag = false;

		for(auto i:aliasdb)
		{
			if(i.first.compare(cmd) == 0)
			{
				aliasFlag = true;
				if(aliasdb[cmd].second == true)
				{
					aliasValid = 1;
				}
				cmd = i.first;
				break;
			}
		}		

		if(aliasFlag == true)
		{	
			if(aliasValid == 0)	
			{
				std::string message = "Error: invalid alias!";	
				int n = write(sock_id_new,message.c_str(),message.length());
	   			if(n <= 0)
	   				error("socket write failed");
				close(sock_id_new);					
				continue;
			}

			string argumentstr = aliasdb[cmd].first;
			std::istringstream iss(argumentstr);
			int size = std::count(argumentstr.begin(), argumentstr.end(), ' ');
			int len = argumentstr.length();
			
			/*char* arguments[size+2];					
			int i = 0;
			string tmp;
			while(iss >> tmp)
			{
				arguments[i] = (char *)malloc(sizeof(char) * len);
				strncpy(arguments[i],tmp.c_str(),tmp.length()+1);
				i++;				
			};	
			arguments[i] = NULL;*/
			dup2(sock_id_new,1);
			close(sock_id_new);						

			k = fork();
			if(k == 0)
			{						 							
				if(system(argumentstr.c_str()) == -1)
				{
					if(execlp("/bin/sh","/bin/sh","-c","echo Error: Please enter a valid alias!",(char*)NULL) == -1)						
						error("execlp failed");
				}
				exit(0);
			}
			else
			{													
				dup2(saved_stdout, 1);
				close(saved_stdout);
			}	
			aliasValid = 0;								
			continue;	
		}

		else if(cmd.compare("ping") == 0)
		{
			dup2(sock_id_new,1);
			close(sock_id_new);						
			k = fork();
			if(k == 0)
			{		
				if (gethostbyname(args.c_str()) == NULL) 				
				{
				    if(execlp("/bin/sh","/bin/sh","-c","echo Error: Please enter a valid IPaddress as argument",(char*)NULL) == -1)
				    	error("execlp failed");				
				}
				else
				{
					if(execlp("/bin/ping","/bin/ping","-c","1", args.c_str(),(char*)NULL) == -1)							
						error("execlp failed");
				}
				exit(0);
			}
			else
			{					
				dup2(saved_stdout, 1);
				close(saved_stdout);
			}									
			continue;
		}

		else if(cmd.compare("exit") == 0)
		{		
			string exitmessage = "Goodbye!";
			int n = write(sock_id_new,exitmessage.c_str(),exitmessage.length());
	   		if(n <= 0)
	   		{
	   			error("socket write failed!");
	   			exit(0);
	   		}

			dup2(sock_id_new,1);
			close(sock_id_new);						
			k = fork();
			if(k == 0)
			{				
				if(execlp("/bin/sh","/bin/sh","-c","echo Goodbye!",(char*)NULL) == -1)
					error("execlp failed");
				exit(0);
			}
			else
			{		
				sessiondb.erase(sidRecv);			
				dup2(saved_stdout, 1);
				close(saved_stdout);
			}									
			continue;		
		}

		else if(cmd.compare("login") == 0)
		{																				
			dup2(sock_id_new,1);
			close(sock_id_new);						
			k = fork();
			if(k == 0)
			{
				if(sessiondb[sidRecv].second != "")
				{
					if(execlp("/bin/sh","/bin/sh","-c","echo Error: Please logout first!",(char*)NULL) == -1)
						error("execlp failed!");
				}				
				else if(userdb[args].empty())			
				{					
					if(execlp("/bin/sh","/bin/sh","-c","echo Error: Not a valid username!",(char*)NULL) == -1)
						error("execlp failed!");
				}				
				else
				{
					if(execlp("/bin/sh","/bin/sh","-c","echo Message: Please enter the password command",(char*)NULL) == -1)
						error("execlp failed!");					
				}
				exit(0);
			}
			else
			{					
				dup2(saved_stdout, 1);
				close(saved_stdout);
				username = args;

				std::string clientInfo,sidRecv,cmd,args;
				getClientInfo(clientInfo,sock_id_new,sock_id,c_addport,sidRecv,cmd,args);				

				if(cmd.compare("pass") == 0 && sidRecv == "")
				{			
					if(userdb[username] == args)
					{
						std::string sid = randomString();								
						// TODO: check how to append base_dir here															
						sessiondb[sid].second = getcwd(NULL,0); 
						sessiondb[sid].first = username;
						std::string loginSuccMsg = "LOGIN SUCCESSFUL$"+sid;	
						int n = write(sock_id_new,loginSuccMsg.c_str(),loginSuccMsg.length());
			   			if(n <= 0)
			   			{
			   				error("socket write failed!");
			   				exit(0);
			   			}
						close(sock_id_new);
					}

					else 
					{												
						std::string message = "Error: passwords do not match";	
						int n = write(sock_id_new,message.c_str(),message.length());
			   			if(n <= 0)
			   			{
			   				error("socket write failed");
			   				exit(0);
			   			}
						close(sock_id_new);												
					}					
				}
				else
				{					
					std::string message;					
					message = "Message: Please enter the password command after login!";
					int n = write(sock_id_new,message.c_str(),message.length());
		   			if(n <= 0)
		   			{
		   				error("socket write failed");
		   				exit(0);
		   			}
					close(sock_id_new);		

				}
				username = "";
				dup2(saved_stdout, 1);
				close(saved_stdout);				
			}									
			continue;
		}	

		else if(cmd.compare("ls") == 0)
		{
			dup2(sock_id_new,1);
			close(sock_id_new);						
			k = fork();			
			if(k == 0)
			{				
				if(sessiondb[sidRecv].second == "")
				{
					if(execlp("/bin/sh","/bin/sh","-c","echo Error: Please login first!",(char*)NULL) == -1)
						error("execlp failed");
				}							
				else
				{					
					if(execlp("ls","ls","-l",sessiondb[sidRecv].second.c_str(),(char*)NULL) == -1)
						error("execlp failed");
				}
				exit(0);
			}
			else
			{					
				dup2(saved_stdout, 1);
				close(saved_stdout);
			}									
			continue;
		}

		else if(cmd.compare("cd") == 0)
		{
			DIR* dir;
			if(args[0] == '/' || args[0] == '~')
			dir = opendir(args.c_str());
			else
			{
			    args = sessiondb[sidRecv].second + '/' + args;
			    dir = opendir(args.c_str());
			}
			dup2(sock_id_new,1);
			close(sock_id_new);						
			k = fork();
			if(k == 0)
			{				
				if(sessiondb[sidRecv].second == "")
				{
					if(execlp("/bin/sh","/bin/sh","-c","echo Error: Please login first!",(char*)NULL) == -1)
						error("execlp failed");
				}							
				else
				{							
					if(args.length() > 1000)	
					{				
					    if(execlp("/bin/sh","/bin/sh","-c","echo Error: File length too big!",(char*)NULL) == -1)
							error("execlp failed");
					}
					else if(dir)	
					{
						if(execlp("/bin/sh","/bin/sh","-c","echo Message: Working directory changed!",(char*)NULL) == -1)
							error("execlp failed");				
					}
					else
					{
						if(execlp("/bin/sh","/bin/sh","-c","echo Error: Enter a valid working directory",(char*)NULL) == -1)	
							error("execlp failed");			
					}
				}
				closedir(dir);
				exit(0);
			}
			else
			{															
				if(dir && args.length() < 1000 && sessiondb[sidRecv].second != "")
				{
					int fileSize = 1000;
					char *path = (char*) malloc(sizeof(char)*fileSize);						
					char *checkptr = realpath(args.c_str(), path);
					if(checkptr != NULL) sessiondb[sidRecv].second = string(path);
					else sessiondb[sidRecv].second = string(path);					
				}				
				closedir(dir);
				dup2(saved_stdout, 1);
				close(saved_stdout);
			}									
			continue;
		}

		else if(cmd.compare("date") == 0)
		{
			dup2(sock_id_new,1);
			close(sock_id_new);						
			k = fork();
			if(k == 0)
			{				
				if(sessiondb[sidRecv].second == "")
				{
					if(execlp("/bin/sh","/bin/sh","-c","echo Error: Please login first!",(char*)NULL) == -1)
						error("execlp failed");
				}							
				else
				{					
					if(execlp("/bin/date","/bin/date",0,(char*)NULL) == -1)
						error("execlp failed");
				}
				exit(0);
			}
			else
			{					
				dup2(saved_stdout, 1);
				close(saved_stdout);
			}									
			continue;
		}
		
		else if(cmd.compare("whoami") == 0)
		{
			dup2(sock_id_new,1);
			close(sock_id_new);						
			k = fork();
			if(k == 0)
			{				
				if(sessiondb[sidRecv].second == "")
				{
					if(execlp("/bin/sh","/bin/sh","-c","echo Error: Please login first!",(char*)NULL) == -1)
						error("execlp failed");
				}							
				else
				{		
					std::string tmp = "echo "+sessiondb[sidRecv].first;
					if(execlp("/bin/sh","/bin/sh","-c",tmp.c_str(),(char*)NULL) == -1)
						error("execlp failed");
				}
				exit(0);
			}
			else
			{					
				dup2(saved_stdout, 1);
				close(saved_stdout);
			}									
			continue;
		}

		else if(cmd.compare("w") == 0)
		{
			dup2(sock_id_new,1);
			close(sock_id_new);						
			k = fork();
			if(k == 0)
			{				
				if(sessiondb[sidRecv].second == "")
				{
					if(execlp("/bin/sh","/bin/sh","-c","echo Error: Please login first!",(char*)NULL) == -1)
						error("execlp failed");
				}							
				else
				{		
					std::string users = "cat << EOF \n";
					for(auto i:sessiondb)
					{
						users += i.second.first + "\n";
					}
					if(execlp("/bin/sh","/bin/sh","-c",users.c_str(),(char*)NULL) == -1)
						error("execlp failed");
				}
				exit(0);
			}
			else
			{					
				dup2(saved_stdout, 1);
				close(saved_stdout);
			}									
			continue;
		}

		else if(cmd.compare("logout") == 0)
		{
			dup2(sock_id_new,1);
			close(sock_id_new);						
			k = fork();
			if(k == 0)
			{				
				if(sessiondb[sidRecv].second == "")
				{
					if(execlp("/bin/sh","/bin/sh","-c","echo Please login first!",(char*)NULL) == -1)
						error("execlp failed");
				}							
				else
				{					
					if(execlp("/bin/sh","/bin/sh","-c","echo You are logged out!",(char*)NULL) == -1)
						error("execlp failed");
				}
				exit(0);
			}
			else
			{	
				sessiondb.erase(sidRecv);
				dup2(saved_stdout, 1);
				close(saved_stdout);
			}									
			continue;
		}

		else if(cmd.compare("get") == 0 || cmd.compare("put") == 0)
		{			
			if(sidRecv == "")
			{
				std::string message;						
				message = "Error: Please use the login command first!";
				int n = write(sock_id_new,message.c_str(),message.length());
				if(n <= 0)
				{
					error("socket write failed!");
					exit(0);
				}
				close(sock_id_new);	
				continue;
			}			

	        int dport = 0;
	        if(portdb.find(sidRecv) == portdb.end())
	        {
	            int k = fork();
	            if(k == 0) 	            
	            	exit(0);	            
	            else 	            
	                dport = k+1024;	            	           
	            std::pair<int, int> np = make_pair(dport, 0);
	            portdb[sidRecv] = np;
	        }
	        
	        dport = portdb[sidRecv].first;
	        std::string filename(args);
	        std::string path = sessiondb[sidRecv].second + "/" + filename;
	        int fsize = fileSize(path.c_str());
		    std::string buf = "";

		    if(cmd.compare("get") == 0)
		    {
		        buf = "get port: $"+ std::to_string(dport) + " size: $" + std::to_string(fsize) + "\n";		        
		    }
		    else 
		    {
		        buf = "put port: $" + std::to_string(dport)+ "\n";
		    }

	        int ret = write(sock_id_new, buf.c_str(), buf.size());
	        if(ret <= 0)
	        {
	        	error("socket write failed!");
	        	exit(0);
	        }
	      
	        if (portdb[sidRecv].second == 0)
	        {
	            errno = 0;
	            ret = 0;
	            struct sockaddr_in server_addr2;
	            bzero((char*)&server_addr2, sizeof(server_addr2));
	          
	            int sd2 = socket(AF_INET, SOCK_STREAM, 0); 
	            
	            if(sd2 == -1)
	            {
	            	error("socket setup failed!");
	            	exit(0);
	            }

	            server_addr2.sin_family = AF_INET;
	            server_addr2.sin_port = htons(dport);
	            server_addr2.sin_addr.s_addr = htonl(INADDR_ANY);
	          
	            int option_val = 1;
	            ret = setsockopt(sd2, SOL_SOCKET, SO_REUSEADDR, (const void*)&option_val, sizeof(int)); 
	            if(ret == -1)
	            {
	            	error("socket setup failed!");
	            	exit(0);
	            }

	            ret = bind(sd2, (struct sockaddr *)&server_addr2, sizeof(server_addr2));
	            if(ret == -1)
	            {
	            	error("socket bind failed!");
	            	exit(0);
	            }
	          
	            ret = listen(sd2, 5); 
	            if(ret == -1)
	            {
	            	error("socket listen failed!");
	            	exit(0);
	            }

	            struct sockaddr_in client_addr2;
	            socklen_t clientaddr2_len = sizeof(client_addr2);

	            int acceptedSocket2 = accept(sd2, (struct sockaddr*)&client_addr2, &clientaddr2_len);
	            if(acceptedSocket2  == -1)
	            {
	            	error("socket accept failed!");
	            	exit(0);
	            }

	            portdb[sidRecv].second = acceptedSocket2;
	        } 
	        else 
	        {  
	            kill(portdb[sidRecv].first, SIGUSR1);	            
	        }

	        int k = fork();
	        if(k == 0)
	        {
	            int blocksize = 100;
	            int acceptedSocket2 = portdb[sidRecv].second;
	            if(acceptedSocket2 <= 0) 
	            {
	                cout << "ERROR: socket not set properly\n";
	                exit(0);
	            }
	            if(cmd.compare("get") == 0) 
	            {
	                int fd = open(path.c_str(), O_RDONLY|O_NONBLOCK);
	                
	                if(fd < 0)
	                { 
	                	error("file open failed!");
	                }

	                if(fd > 0) 
	                {
	                      int c;
	                      char * buf2 = (char*) malloc(sizeof(char)*blocksize);
	                      while(((c = read(fd, buf2, blocksize)) > 0) && cancel_download != 1)
	                      {
	                          write(acceptedSocket2, buf2, c);
	                          if(c < blocksize)
	                              break;
	                      }
	                      free(buf2);
	                  }
	                  close(fd);
	            } 
	            else 
	            {
                  //printf("[793] open on %s\n", path.c_str());
	                int fd = open(path.c_str(), O_CREAT | O_EXCL | O_RDWR, 0644);
	                if(fd < 0)
	                {
	                    error("file open failed!");	
	                }
	                  
	                if(fd > 0 )
	                {
                      	int c;
	                    char * buf2 = (char*) malloc(sizeof(char)*blocksize);
	                    while(((c = read(acceptedSocket2, buf2, blocksize)) > 0) && cancel_download != 1)
                      	{
	                    	write(fd, buf2, c);
		                    if(c < blocksize)
		                        break;
		                }
		                free(buf2);
		            }
	                close(fd);
	            }
	            
	            portdb[sidRecv].second = 0;
	            close(acceptedSocket2);
	            exit(0);
	        }
	    }

		else
		{		
			std::string message = "Error invalid command";	
			int n = write(sock_id_new,message.c_str(),message.length());
   			if(n <= 0)
   			{
   				error("socket write failed!");
   				exit(0);
   			}
			close(sock_id_new);	
			continue;		
		}

		struct sigaction sa;
		sa.sa_handler = &handler;
		sigemptyset(&sa.sa_mask);
		sa.sa_flags = SA_RESTART | SA_NOCLDSTOP;
		if(sigaction(SIGCHLD, &sa, 0) != -1);
	}
		
	close(sock_id);		
	return 0;
}
