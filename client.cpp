#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/stat.h>

#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <map>
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <cassert>
#include <bits/stdc++.h>

using namespace std;

void error(string message)
{
    cout<<"Error: "<<message<<endl;    
}

int get_port(string s){
    int pstart = s.find('$');
    s = s.substr(pstart+1);
    int pend = s.find_first_of(' ');
    s = s.substr(0,pend);    
    return atoi(s.c_str());
}

int main(int argc, char* argv[])
{
    std::ifstream in;
    std::ofstream out;
    std::streambuf *cinbuf, *coutbuf;

	if(argc<3)
	{
		cout<<"Error: correct usage - %s hostname portnumber\n"<<argv[0];
		exit(1);
	}
    else if(argc == 5)
    {
        in.open(argv[3],std::ifstream::in);
        cinbuf = std::cin.rdbuf(); 
        std::cin.rdbuf(in.rdbuf());

        out.open(argv[4],std::ofstream::out);
        coutbuf = std::cout.rdbuf(); 
        std::cout.rdbuf(out.rdbuf());
    }
	
	map<int,int> inuse;    
	int sock_id,port_no,n;
	struct sockaddr_in s_addport;	
	struct hostent *server;	
    port_no = atoi(argv[2]);
	string sid = "";

	while(1)
	{				
		string clientInfo;
		getline(cin,clientInfo);		

		clientInfo = sid+"$"+clientInfo;
		string sidRecv,cmd,args;        
	    int pos1 = clientInfo.find_first_of("$");
	    sidRecv = clientInfo.substr(0,pos1);
	    int pos2 = clientInfo.find_first_of("$",pos1+1);
	    cmd = clientInfo.substr(pos1+1,pos2-pos1-2);        
	    args = clientInfo.substr(pos2+1);
	    
		if(cmd.compare("logout") == 0) sid = "";			
		server = gethostbyname(argv[1]);
		if(server == NULL)
		{
			error("unable to resolve hostname!");
			exit(1);
		}

		sock_id = socket(AF_INET, SOCK_STREAM, 0);
		if(sock_id <= 0)    
        {    
            error("socket connection issue");        
            exit(0);
        }

		bzero((char *) &s_addport, sizeof(s_addport));
		s_addport.sin_family = AF_INET;
		s_addport.sin_port = htons(port_no);
		bcopy((char *)server->h_addr, 
	         (char *)&s_addport.sin_addr.s_addr,
	         server->h_length);
		if(connect(sock_id,(struct sockaddr *) &s_addport,sizeof(s_addport)) !=0)
        {
            error("connect failed!");
            exit(0);
        }

        if(write(sock_id,clientInfo.c_str(),clientInfo.length()) <= 0)
        {
            error("write to socket failed!");
            exit(0);
        }

        if((cmd.compare("get") == 0 || cmd.compare("put") == 0) && sid != "")
        {
            string fname = args;
            int get, put;
            if (cmd.compare("get") == 0) 
            {
                get = 1;
                put = 0;
            } 
            else 
            {
                get = 0;
                put = 1;
            }

            int sd = sock_id;            
            fcntl(sd, F_SETFL, O_RDONLY);

            char buf2[100];
            memset(buf2, 0, 100);
            if(read(sd, buf2, 100) <= 0)
            {
                error("read from socket failed");
                exit(0);
            }
            string portinfo(buf2);            

            int nport = get_port(portinfo);            
            int sd2 = 0;

            if(inuse.find(nport) == inuse.end())
            {
                inuse[nport] = 0;
            }

            if(inuse[nport] == 0)
            {
                sd2 = socket(AF_INET, SOCK_STREAM, 0);
                if(sd2 == -1)
                {
                    error("socket connection failed");
                    exit(0);
                }
                struct sockaddr_in server_addr2;
                bzero((char*)&server_addr2, sizeof(server_addr2));
                server_addr2.sin_family = AF_INET;
                bcopy((char*)server->h_addr, (char*)&server_addr2.sin_addr.s_addr, server->h_length);

                server_addr2.sin_port = htons(nport);
                if(connect(sd2, (struct sockaddr*)&server_addr2, sizeof(server_addr2)) == -1)
                {
                    error("connect failed");
                    exit(0);
                }
                inuse[nport] = sd2;
            } 
            else 
            {
                sd2 = inuse[nport];
            }

            int blocksize = 100;
            char * buf  = (char*) malloc(sizeof(char)*blocksize);
            int ret = 0; 
            unsigned long long int numbytes = 0; 

            if(get == 1 && put == 0) 
            {
                fcntl(sd2, F_SETFL, O_RDONLY);
                int fd = open(fname.c_str(), O_CREAT | O_EXCL | O_RDWR, 0644);
                
                if(fd < 0) 
                {
                    error("opening file error!");
                }
                
                if(fd > 0 )
                {
                    int k = fork();
                    if(k == 0) 
                    {
                        while((ret = read(sd2, buf, blocksize)) > 0) 
                        {
                            write(fd, buf, ret);
                            numbytes += ret;
                            if(ret < blocksize)
                                break;
                        }
                        inuse[nport] = 0;
                    } 
                    else 
                    {
                        continue;
                    }
                }
                close(fd);
            } 
            else 
            {
                struct stat statbuf; 
                int filestatus = stat(fname.c_str(), &statbuf);
                if(filestatus == -1)
                {
                    error("stat failed!");
                }
                
                int fd = open(fname.c_str(), O_RDONLY|O_NONBLOCK);
                if(fd < 0)  
                {
                    error("opening file failed!");                
                }
                
                if(fd > 0)
                {
                    while((ret = read(fd, buf, blocksize)) > 0) 
                    {
                        write(sd2, buf, ret);
                        numbytes += ret;
                        if(ret < blocksize) 
                        {
                            break;
                        }
                    }
                }
                inuse[nport] = 0;
                close(fd);
            }            
            
            free(buf);
            close(sd);
            close(sd2);
        } 
        else 
        {
            char *buffer  = (char*) malloc(sizeof(char)*10000);
            bzero(buffer,10000);
            if(read(sock_id,buffer,10000) <= 0)
            {
                error("read failed!");
                exit(0);
            }
              
            string msg = string(buffer);	
            if(msg == "Goodbye!") 
            {            	
            	close(sock_id);
                if(argc == 5)
                {
                    std::cin.rdbuf(cinbuf);   
                    std::cout.rdbuf(coutbuf);
                    in.close();
                    out.close();
                }
            	return 0;
            }
            
            int pos = msg.find("LOGIN SUCCESSFUL$",0);
            if(pos == string::npos)
                cout<<msg<<endl;
            else
            {	
                sid.clear();    	
                sid = msg.substr(17);
                cout<<"Login Succesful!\n"<<endl;
            }
        }
        close(sock_id);
    }

    if(argc == 5)
    {
        std::cin.rdbuf(cinbuf);   
        std::cout.rdbuf(coutbuf);
        in.close();
        out.close();
    }

	return 0;
}
